package com.geoscan.pool;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

public class Main {
    public static void main(String[] args) {
        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);
        GLCanvas glcanvas = new GLCanvas(capabilities);
        PoolGL pool = new PoolGL();

        glcanvas.addGLEventListener(pool);
        glcanvas.addMouseWheelListener(pool.getMouseWheelListener());
        glcanvas.addMouseMotionListener(pool.getMouseMoitionListener());
        glcanvas.addMouseListener(pool.getMouseListener());
        glcanvas.setSize(800, 480);
        PoolJFrame poolJFrame = new PoolJFrame("Geoscan  Anton Kormschikov", glcanvas, pool);
        FPSAnimator animator = new FPSAnimator(glcanvas, 300, true);
        animator.start();
    }

}