package com.geoscan.pool;

import java.awt.DisplayMode;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

public class PoolGL implements GLEventListener {
    private float xRotation = 0.0f;
    private float yRotation  = 0.0f;
    private float zCamera = 15.0f;
    private float lastX = 0.0f;
    private float lastY = 0.0f;
    private float rotationSpeed = 0.5f;

    private final PoolCalculate poolCalculate = new PoolCalculate();
    private int[] blocksHeight;
    private int[] waterHeight;


    private final float blockSize = 1.0f;

    public static DisplayMode dm, dm_old;
    private GLU glu = new GLU();
    int stoneTexture;
    int waterTexture;

    /**
     * Listeners creation
     */
    private final MouseWheelListener mouseWheelListener = new MouseWheelListener() {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (zCamera >= 0) {
                zCamera += e.getWheelRotation();
            } else {
                zCamera = 0.0f;
            }
        }
    };
    private final MouseMotionListener mouseMoitionListener = new MouseMotionListener() {
        public void mouseDragged(MouseEvent e){
            yRotation += (lastX - e.getX()) * rotationSpeed;
            xRotation += (lastY - e.getY()) * rotationSpeed;
            lastX = e.getX();
            lastY = e.getY();
        }
        public void mouseMoved(MouseEvent e){
        }
    };
    private final MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
        }
        @Override
        public void mousePressed(MouseEvent e) {
            lastX = e.getX();
            lastY = e.getY();
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            lastX = 0;
            lastY = 0;
        }
        @Override
        public void mouseEntered(MouseEvent e) {
        }
        @Override
        public void mouseExited(MouseEvent e) {
        }
    };

    /**
     * Listener getters
     *
     */
    public MouseWheelListener getMouseWheelListener(){
        return mouseWheelListener;
    }
    public MouseMotionListener getMouseMoitionListener() {
        return mouseMoitionListener;
    }
    public MouseListener getMouseListener() {
        return mouseListener;
    }


    public PoolGL(){
        // Set default value
        blocksHeight = new int[]{1,2,4,2,1,5,4,7};
        waterHeight = poolCalculate.calculate(blocksHeight);
    }
    public boolean setBlocksAndGenWater(int[] blocksHeight){
        for (int el : blocksHeight){
            if (el < 0){
                return false;
            }
        }
        if (blocksHeight != null && blocksHeight.length > 0) {
            this.blocksHeight = blocksHeight;
            waterHeight = poolCalculate.calculate(blocksHeight);
            return true;
        }
        return false;
    }
    public boolean setBlocksAndGenWater(File file){
        List<Integer> blocksHeight = new ArrayList<>();
        if (file != null) {
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextInt()) {
                    blocksHeight.add(scanner.nextInt());
                }
            } catch (IOException e) {
                System.out.printf("File %s not found\n", file);
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return setBlocksAndGenWater(blocksHeight.stream().mapToInt(i -> i).toArray());
    }
    private void drawCube(GL2 gl, int texture){
        float sz = blockSize/2;
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS); // Start Drawing The Cube
        // Front face
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(-sz, -sz, sz);
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(sz, -sz, sz);
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(sz, sz, sz);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(-sz, sz, sz);
        // Back Face
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(-sz, -sz, -sz);
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(-sz, sz, -sz);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(sz, sz, -sz);
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(sz, -sz, -sz);
        // Top Face
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(-sz, sz, -sz);
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(-sz, sz, sz);
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(sz, sz, sz);
        gl.glTexCoord2f(1.0f, 1.0f);  gl.glVertex3f(sz, sz, -sz);
        // Bottom Face
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(-sz, -sz, -sz);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(sz, -sz, -sz);
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(sz, -sz, sz);
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(-sz, -sz, sz);
        // Right face
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(sz, -sz, -sz);
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(sz, sz, -sz);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(sz, sz, sz);
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(sz, -sz, sz);
        // Left Face
        gl.glTexCoord2f(0.0f, 0.0f); gl.glVertex3f(-sz, -sz, -sz);
        gl.glTexCoord2f(1.0f, 0.0f); gl.glVertex3f(-sz, -sz, sz);
        gl.glTexCoord2f(1.0f, 1.0f); gl.glVertex3f(-sz, sz, sz);
        gl.glTexCoord2f(0.0f, 1.0f); gl.glVertex3f(-sz, sz, -sz);
        gl.glEnd(); // Done Drawing The Quad
    }
    @Override
    public void display( GLAutoDrawable drawable ) {
        final GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        glu.gluLookAt(0.0f, 0.0f, zCamera, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);
        gl.glRotatef(xRotation, -1.0f, 0.0f, 0.0f);
        gl.glRotatef(yRotation, 0.0f, -1.0f, 0.0f);
        int maxHeight = 0;
        for (int element : blocksHeight){
            if (element > maxHeight){
                maxHeight = element;
            }
        }
        float dx = -(blocksHeight.length*blockSize)/2.0f + blockSize/2;
        float dy = -(maxHeight*blockSize)/2.0f + blockSize/2;
        gl.glTranslatef(dx, dy, 0.0f);
        for (int i = 0 ; i < blocksHeight.length ; i ++){
            gl.glPushMatrix();
            for (int blockNumber = 0; blockNumber < blocksHeight[i]; blockNumber++){
                drawCube(gl, stoneTexture);
                gl.glTranslatef(0.0f, blockSize, 0.0f);
            }
            for (int waterBlockNumber = 0; waterBlockNumber < waterHeight[i]; waterBlockNumber++){
                drawCube(gl, waterTexture);
                gl.glTranslatef(0.0f, blockSize, 0.0f);
            }
            gl.glPopMatrix();
            gl.glTranslatef(blockSize, 0.0f, 0.0f);
        }
        gl.glFlush();
    }

    @Override
    public void dispose( GLAutoDrawable drawable ) {
        // TODO Auto-generated method stub
    }

    @Override
    public void init( GLAutoDrawable drawable ) {

        final GL2 gl = drawable.getGL().getGL2();
        gl.glShadeModel(GL2.GL_SMOOTH);
        gl.glClearColor(1f, 1f, 1f, 0f);
        gl.glClearDepth(1.0f);
        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glDepthFunc(GL2.GL_LEQUAL);
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnable(GL2.GL_TEXTURE_2D);
        try{
            Texture t;
            File stoneTextureFile = new File("textures\\_stone.png");
            t = TextureIO.newTexture(stoneTextureFile, true);
            stoneTexture= t.getTextureObject(gl);
            File waterTextureFile = new File("textures\\_water.png");
            t = TextureIO.newTexture(waterTextureFile, true);
            waterTexture = t.getTextureObject(gl);

        }catch(IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public void reshape( GLAutoDrawable drawable, int x, int y, int width, int height ) {
        final GL2 gl = drawable.getGL().getGL2();
        final float h = (float) width / ( float ) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(55.0f, h, 1.0, 100.0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

}