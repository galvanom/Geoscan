package com.geoscan.pool;

import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class PoolJFrame extends JFrame {
    private final PoolGL pool;
    private File currentFile;

    public PoolJFrame(String windowName, GLCanvas glCanvas, PoolGL pool){
        super(windowName);
        this.pool = pool;
        setJMenuBar(getJMenu());
        getContentPane().add(glCanvas);
        setSize(getContentPane().getPreferredSize());
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    private JMenuBar getJMenu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu viewMenu = new JMenu("View");
        JMenuItem fileAddItem = new JMenuItem("Set file");
        JMenuItem reloadItem = new JMenuItem("Reload file");
        fileMenu.add(fileAddItem);
        viewMenu.add(reloadItem);
        menuBar.add(fileMenu);
        menuBar.add(viewMenu);
        fileAddItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnVal = fileChooser.showOpenDialog(fileChooser);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    if (!pool.setBlocksAndGenWater(fileChooser.getSelectedFile())) {
                        System.out.println("Couldn't load the file");
                    } else {
                        currentFile = fileChooser.getSelectedFile();
                    }
                }
            }
        });
        reloadItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentFile != null) {
                    if (!pool.setBlocksAndGenWater(currentFile)) {
                        System.out.println("Couldn't reload the file");
                    }
                }
            }
        });
        return  menuBar;
    }
}
