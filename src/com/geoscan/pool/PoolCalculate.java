package com.geoscan.pool;
public class PoolCalculate {
    private int blocksHeight[];
    private int waterHeight[];

    private int[] calculateSlice(int leftEdge, int rightEdge){
        int leftMax = 0;
        int rightMax = 0;
        int leftPointer = leftEdge;
        int rightPointer = rightEdge;

        while (leftPointer < rightPointer){
            if (blocksHeight[leftPointer] > leftMax){
                leftMax = blocksHeight[leftPointer];
            }
            if (blocksHeight[rightPointer] > rightMax){
                rightMax = blocksHeight[rightPointer];
            }
            if (leftMax >= rightMax){
                waterHeight[rightPointer] = rightMax - blocksHeight[rightPointer];
                rightPointer--;
            }
            else{
                waterHeight[leftPointer] = leftMax - blocksHeight[leftPointer];
                leftPointer++;
            }
        }

        return waterHeight;
    }
    private void divideAndConquer(){
        int leftEdge = 0;
        int rightEdge = 0;

        while (leftEdge < blocksHeight.length){
            while (rightEdge < blocksHeight.length){
                if (blocksHeight[rightEdge] == 0){
                    while (rightEdge < blocksHeight.length && blocksHeight[rightEdge] == 0){
                        rightEdge++;
                    }
                    break;
                }
                else {
                    rightEdge++;
                }
            }
            calculateSlice(leftEdge, rightEdge - 1);

            leftEdge = rightEdge;
        }
    }
    public int[] calculate(int[] blocksHeight){
        this.blocksHeight = blocksHeight;
        waterHeight = new int[blocksHeight.length];
        divideAndConquer();
        return waterHeight;
    }
}
